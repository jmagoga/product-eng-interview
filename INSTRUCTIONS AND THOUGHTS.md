# To run project:

nix-shell
python3 index.py
cd frontend
npm install
yarn start

# About the app:

The frontend and backend responsibilities are pretty much even. The decisions to whether a http request is done or not is mostly based on the object *allSdksInfo* on the frontend. It is the source of all truth in the app. It is the object checked to see whether two different sdks need to be compared (with request to API) or not. 

There are two main API endpoints: one that gets 'general info' about sdks and another that gets 'comparative data' between two sdks. In this way, no operation needs to be done twice.


# Inferences about the data

I had questions about how to interpret the DB data, but instead of reaching out to you I decided to make some inferences following Occam's razor and making things as simple as possible. In case the inferences are equivocated, I created a file called 'alternative_code' which contains Python and SQL code which interpret the database in a different way. Those shouldn't take long to be plugged in. *I would like to clarify these with you on our next call*

One example is getting the value of apps that 'haven't integrated any of these three payments SDKs'. Does this mean the app has NEVER integrated (there's no relation on app_sdk between the two) or is NOT CURRENTLY integrated (the relation can exist but is one with installed = 0 ?)

Another example is 'acquired 11,844 app integrations from another solution not covered in this matrix'. I applied simple math to it (subtracting from the total num of sdks the num of churns to the other sdks present in this matrix). I thought it could otherwise be related to the 'installed' property of app_sdk. There's code for that on 'alternative_code.py'

# Bugs

Mainly related to the above inferences. Some SQL queries might be innacurate.
When the number of 'MATRIX_COMPARISONS' is changed from 3 to another number, the 'alerts' might be wrong. The matrix should work fine, though.
Some negatives numbers used to show.

# Thoughts

Most of my thoughts were into whether I should build an 'API first app' or not. Since in our first conversation you asked if I'd had previous Python exposure, I've also built into the API returns some key-value pairs which are not used in this app but could be used for other situations. e.g. 'acquired_from'

# Deficiencies

Questions about how to interpret DB; more complex queries are in file 'alternative_code.py'
There's one data point that gets repeated: sdks names. They are repeated in the API in case the app is designed with no previous SDK selector. Could be easily removed.
Normalization by row not built yet.

# Overall, I had a lot of fun building this and I'm excited to hear what you think!

I built this with flask and create-react-app as those are, for me, the quickest way to get something up and running.