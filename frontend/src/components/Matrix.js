import React, { useEffect, useState } from "react";
import MatrixSquare from "./MatrixSquare";
import { merge } from "lodash";

function Matrix({ selectedSdks, allSdksInfo, setAllSdksInfo, numComparisons }) {
  const [sdksIds, setSdksIds] = useState([]);
  const [showMatrix, setShowMatrix] = useState(false);
  const [showSampleApps, setShowSampleApps] = useState(false);

  function onMouseEnter(id) {
    if (!id) return;
    if (allSdksInfo?.[id]?.hasOwnProperty("sample_apps")) {
      setShowSampleApps(id);
    } else {
      fetch(`/api/sdk/sample_apps?sdk_id=${id}`)
        .then((res) => res.json())
        .then((res) => {
          const data = res;
          let objCopy = Object.assign({}, allSdksInfo);
          objCopy = merge(objCopy, data);
          setAllSdksInfo(objCopy);
          setShowSampleApps(id);
        });
    }
  }

  useEffect(() => {
    (async function () {
      const selectedSdksKeys = Object.values(selectedSdks);
      setSdksIds(selectedSdksKeys);
      if (!selectedSdksKeys.length) return;

      setShowMatrix(false);

      let comparedCount = 0;

      const notComparedKeys = [];

      selectedSdksKeys.forEach((key, i) => {
        for (let j = i + 1; j < selectedSdksKeys.length; j++) {
          if (
            allSdksInfo?.[key]?.["churned_to"]?.hasOwnProperty(
              selectedSdksKeys[j]
            )
          ) {
            comparedCount += 1;
            if (comparedCount === numComparisons) {
              setShowMatrix(true);
            }
          } else {
            notComparedKeys.push([key, selectedSdksKeys[j]]);
          }
        }
      });

      let objCopy = Object.assign({}, allSdksInfo);

      for (const [id1, id2] of notComparedKeys) {
        const res = await fetch(`/api/sdk/churn?sdk1_id=${id1}&sdk2_id=${id2}`);
        const data = await res.json();

        const copyPreviousObj = Object.assign({}, objCopy);

        objCopy = merge(objCopy, data);

        const resKeys = Object.keys(data);

        for (const key of resKeys) {
          const new_churned_to = objCopy[key]["churned_to"];
          const new_acquired_from = objCopy[key]["acquired_from"];

          if (copyPreviousObj[key]) {
            objCopy[key]["churned_to"] = {
              ...new_churned_to,
              ...copyPreviousObj[key]["churned_to"],
            };
            objCopy[key]["acquired_from"] = {
              ...new_acquired_from,
              ...copyPreviousObj[key]["acquired_from"],
            };
          }
        }
      }

      for (const id of selectedSdksKeys) {
        if (!allSdksInfo[id]?.["num_installed_apps"]) {
          const res = await fetch(`/api/sdk/general_info?sdk_id=${id}`);
          const data = await res.json();
          objCopy[id]["num_installed_apps"] = data["num_installed_apps"];
          objCopy[id]["acquired_total"] = data["acquired_total"];
          objCopy[id]["churned_total"] = data["churned_total"];
          objCopy[id]["name"] = data["name"];
        }
      }
      setAllSdksInfo(objCopy);
      setShowMatrix(true);
    })();
    // eslint-disable-next-line
  }, [selectedSdks]);

  function displayMatrix() {
    const matrixSquares = [];
    const outerArr = [];

    for (let i = 0; i < numComparisons; i++) {
      const reorderedIds = [...sdksIds];

      if (i !== 0) {
        const removed = reorderedIds.splice(0, i);
        reorderedIds.push(...removed);
      }

      let val;
      const innerArr = [];
      let churned_total = allSdksInfo[reorderedIds[0]]["churned_total"];

      for (let j = 0; j < numComparisons; j++) {
        if (j === 0) {
          val = allSdksInfo[reorderedIds[0]]["num_installed_apps"];
        } else {
          val = allSdksInfo[reorderedIds[0]]["churned_to"][reorderedIds[j]];
          churned_total -= val;
        }
        const innerArrPosition =
          i + j < numComparisons ? i + j : (i + j) % numComparisons;
        innerArr[innerArrPosition] = val;
      }
      innerArr.push(churned_total);
      outerArr.push(innerArr);
    }

    let totalNumApps = allSdksInfo["total_num_apps"];

    const arr = [];

    for (let i = 0; i < outerArr.length; i++) {
      let sdkTotalInitial = outerArr[i][i];
      let sdkTotal = sdkTotalInitial;
      for (let j = 0; j < outerArr.length; j++) {
        if (outerArr[j][i] !== sdkTotalInitial) {
          sdkTotal -= outerArr[j][i];
        }
      }
      totalNumApps -= sdkTotalInitial;
      arr.push(sdkTotal);
    }

    const lastRow = [...arr, totalNumApps];

    outerArr.push(lastRow);

    outerArr.forEach((row, j) => {
      // adds: 1) app names, 2) last column, 3) creates MatrixSquares
      const numbers = [];
      const squareName =
        j !== numComparisons && allSdksInfo[selectedSdks[j]]
          ? allSdksInfo[selectedSdks[j]].name
          : "(none)";

      // pushes sdk names
      numbers.push(<MatrixSquare key={`name-${j}`} name number={squareName} />);

      row.forEach((number, i) => {
        numbers.push(
          <MatrixSquare
            key={i}
            id={sdksIds[j]}
            number={number}
            total={allSdksInfo["total_num_apps"]}
            onMouseEnter={() => onMouseEnter(sdksIds[j])} // pegar sdk_ids com as selectedSdks ? fazer um click aqui que faca 'merge' das infos e um 'alert'
            onMouseLeave={() => setShowSampleApps(false)}
          />
        );
      });
      matrixSquares.push(<div key={`div-${j}`}>{numbers}</div>);
    });

    return matrixSquares;
  }

  return (
    <div>
      {showMatrix && (
        <>
          {displayMatrix()}
          <br />
          {showSampleApps &&
            "Apps which use this SDK: " +
              allSdksInfo[showSampleApps]["sample_apps"].join(", ")}
        </>
      )}
    </div>
  );
}

export default Matrix;