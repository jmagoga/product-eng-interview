let pkgs = (import <nixpkgs> {}); in
let stdenv = pkgs.stdenv; in
pkgs.mkShell rec {
    name = "interview";
    shellHook = ''
        source .bashrc
    '';
    buildInputs = (with pkgs; [
        bashInteractive
        (python3.withPackages (p: [p.flask-cors p.ipython p.nose p.sqlalchemy]))
    ]);
}